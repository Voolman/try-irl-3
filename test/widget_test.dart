// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:pinput/pinput.dart';
import 'package:training6/auth/data/models/FavoriteModelNews.dart';
import 'package:training6/auth/data/models/ModelNews.dart';
import 'package:training6/auth/data/models/PlatformModels.dart';
import 'package:training6/auth/data/repository/for_test.dart';
import 'package:training6/auth/data/storage/favorite_news.dart';
import 'package:training6/auth/domain/ArticlePresenter.dart';
import 'package:training6/auth/domain/FavoritePresenter.dart';
import 'package:training6/auth/presentation/pages/Article.dart';
import 'package:training6/auth/presentation/pages/MainPages/Favorite.dart';

import 'package:training6/common/app.dart';

import 'package:training6/main.dart';

void main() async {
  group("", () {
    testWidgets("иконка кнопки «Убрать из избранного» меняется на «Добавить в избранное» при нажатии", (tester) async {
      await initializeDateFormatting("ru");
      await tester.runAsync(() => tester.pumpWidget(
          MyApp(
            body: MaterialApp(
              home: ArticlePage(
                  news: testNews[0],
                  platform: testPlatforms[0],
                  presenterParam: TestArticlePresenter(),
                  initialIndex: 2,
              ),
            )
        )
      ));

      final noFavoriteIcon = find.byIcon(Icons.bookmark_border_outlined);
      expect(noFavoriteIcon, findsOneWidget);

      await tester.tap(noFavoriteIcon);
      await tester.pumpAndSettle();

      final favoriteIcon = find.byIcon(Icons.bookmark);
      expect(favoriteIcon, findsOneWidget);
    });
    
    test('при удалении любой новости из избранного и ее последующем добавлении она оказывается в начале списка', (){
      TestFavoriteTabPresenter presenter = TestFavoriteTabPresenter();
      var data = presenter.fetchData();
      var originLength =  data.length;
      for(var i=0; i < originLength; i++){
        var modelFavoriteNews = data[i];
        presenter.unFavoriteNews(modelFavoriteNews.news);
        expect(favoriteNews.length, originLength-1);
        presenter.addFavoriteNews(modelFavoriteNews);
        expect(favoriteNews.first, modelFavoriteNews);
      }
    });
    
    test('проверить правильность порядка расположения элементов в списке избранных новостей', (){
      TestFavoriteTabPresenter presenter = TestFavoriteTabPresenter();
      var data = presenter.fetchData();
      for (var i=0; i < testFavoriteNews.length; i++){
        var favorite = testFavoriteNews[i];
        var needIndex = testFavoriteNews.length - i;
        expect(favorite, data[needIndex]);
      }
    });
    test('проверьте, что элемент добавляется в начало списка избранных', ()async{
      TestFavoriteTabPresenter presenter = TestFavoriteTabPresenter();
      var data = presenter.fetchData();
      var favorite = FavoriteModelNews(
          news: ModelNews(
              id: 'id_news',
              title: 'Title',
              text: 'text',
              type: 'none',
              channel: 'test',
              idPlatform: 'id_platform',
              publishedAt: DateTime.now()),
          platform: ModelPlatform(
              id: "id_platform",
              icon: "icon",
              title: "Title",
              channels: "test",
              availableChannels: 'test'),
          dateTimeAdded: DateTime(2010)
      );
      data = await presenter.addFavoriteNews(favorite);
      expect(data.first, favorite);
    });
    testWidgets('реализуйте тест на удаление элементов с помощью кнопки «Удалить» до полной отчистки списка;', (tester) async {
      await initializeDateFormatting("ru");
      TestFavoriteTabPresenter presenter = TestFavoriteTabPresenter();
      var tab = Favorite();
      await tester.runAsync(() => tester.pumpWidget(MyApp(
          body: MaterialApp(
            home: tab
          )
      )));
      var origin = tab.data.length;
      for (var i=0; i<origin; i++){
        await tester.pumpAndSettle();
        var beforeRemove = tab.data.length;
        GestureDetector removeButton = find.byKey(Key('remove-btn-0')).evaluate().first.widget as GestureDetector;
        removeButton.onTap!();
        var afterRemove = tab.data.length;
        expect(beforeRemove-1, afterRemove);
      }
    });
    test('проверьте, что при удалении случайного элемента списка избранных новостей, список уменьшается на 1 до полной отчистки', ()async{
      TestFavoriteTabPresenter presenter = TestFavoriteTabPresenter();
      var data = presenter.fetchData();
      var random = Random();
      var originLength = data.length;
      for(var i=0; i<originLength; i++){
        var randomIndex = random.nextInt(data.length);
        var model = data[randomIndex];
        var beforeRemove = data.length;
        data = await presenter.unFavoriteNews(model.news);
        var afterRemove = data.length;
        expect(beforeRemove, afterRemove);
      }
      expect(data.length, 0);
    });
  });
}
