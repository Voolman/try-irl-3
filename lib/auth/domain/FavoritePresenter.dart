import 'package:training6/auth/data/models/ModelNews.dart';
import 'package:training6/auth/data/repository/for_test.dart';
import 'package:training6/auth/data/storage/favorite_news.dart';

import '../data/models/FavoriteModelNews.dart';
import '../data/repository/cache.dart';

abstract class BaseFavoritePresenter{
  List<FavoriteModelNews> fetchData();
  Future<List<FavoriteModelNews>> unFavoriteNews(ModelNews news);
}

class TestFavoriteTabPresenter extends BaseFavoritePresenter{
  @override
  List<FavoriteModelNews> fetchData() {
    return favoriteNews.reversed.toList();
  }

  @override
  Future<List<FavoriteModelNews>> unFavoriteNews(ModelNews news) async {
    favoriteNews.clear();
    favoriteNews.addAll(testFavoriteNews.reversed);
    return favoriteNews;
  }

  Future<List<FavoriteModelNews>> addFavoriteNews(FavoriteModelNews news) async {
    favoriteNews.add(news);
    return favoriteNews.reversed.toList();
  }
}


class FavoriteTabPresenter extends BaseFavoritePresenter{
  @override
  List<FavoriteModelNews> fetchData() {
    return favoriteNews.reversed.toList();
  }

  @override
  Future<List<FavoriteModelNews>> unFavoriteNews(ModelNews news) async {
    removeFavoriteNews(news);
    return fetchData();
  }
}
