import 'package:training6/auth/data/repository/cache.dart';
import 'package:training6/auth/data/storage/favorite_news.dart';
import '../data/models/ModelNews.dart';
import '../data/models/PlatformModels.dart';

abstract class BaseArticlePresenter {
  Future<bool> pressFavorite(ModelNews news, ModelPlatform platform);
  bool checkIsFavorite(String idNews);
}

class TestArticlePresenter extends BaseArticlePresenter{
  bool isFavorite = false;
  @override
  bool checkIsFavorite(String idNews) {
    return isFavorite;
  }

  @override
  Future<bool> pressFavorite(ModelNews news, ModelPlatform platform) async {
    isFavorite = !isFavorite;
    return isFavorite;
  }
}

class ArticlePresenter extends BaseArticlePresenter{
  @override
  Future<bool> pressFavorite(ModelNews news, ModelPlatform platform) async {
    if (checkIsFavorite(news.id)){
      await removeFavoriteNews(news);
      return false;
    }else{
      addFavoriteNews(news, platform);
      return true;
    }
  }

  @override
  bool checkIsFavorite(String idNews) {
    var myList = favoriteNews.where((element) => element.news.id == idNews);
    if (myList.length == 1){
      return true;
    }else{
      return false;
    }
  }
}