import 'package:supabase_flutter/supabase_flutter.dart';
import '../data/repository/supabase.dart';


Future<void> pressSignUp(String email, String password, Function onResponse, Function onError) async {
  try{
    await signUp(email, password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }
}
