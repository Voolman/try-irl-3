import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training6/auth/data/models/PlatformModels.dart';

import '../data/models/ModelNews.dart';
import '../data/repository/supabase.dart';

void fetchDataSearch(Function(List<ModelNews>,List<ModelPlatform>) onResponse, Function(String) onError){
  getAllPlatforms().then((value) async {
    List<ModelPlatform> platform = value;
    try{
      var response = await getNews();
      var result = response.where((element) => element.geoLat != null).toList();
      onResponse(result, platform);
    }on AuthException catch(e){
      onError(e.message);
    }on PostgrestException catch(e){
      onError(e.toString());
    }on Exception catch(e){
      onError(e.toString());
    }
  });
}
