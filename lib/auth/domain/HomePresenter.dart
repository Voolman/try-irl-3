import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training6/auth/data/repository/supabase.dart';

import '../data/models/ModelNews.dart';
import '../data/models/PlatformModels.dart';

class HomePresenter{
  List<ModelPlatform> platform = [];
  List<ModelNews> news = [];
  
  void fetchData(Function(List<ModelNews>, List<ModelPlatform>) onResponse, Function(String) onError){
    getAllPlatforms().then((value) async {
      List<ModelPlatform> platform = value;
      try{
        var result = await getNews();
        onResponse(result, platform);
      }on AuthException catch(e){
        onError(e.message);
      }on PostgrestException catch(e){
        onError(e.toString());
      }on Exception catch(e){
        onError(e.toString());
      }
    });
  }
}