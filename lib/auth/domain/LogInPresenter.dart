import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/repository/supabase.dart';

Future<void> pressSignIn(String email, String password, Function onResponse, Function onError) async {
  try{
    await signIn(email, password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }
}
