import 'package:training6/auth/data/models/ModelNews.dart';
import 'package:training6/auth/data/models/PlatformModels.dart';

class FavoriteModelNews{
  final ModelNews news;
  final ModelPlatform platform;
  final DateTime dateTimeAdded;

  FavoriteModelNews({required this.news, required this.platform, required this.dateTimeAdded});
}