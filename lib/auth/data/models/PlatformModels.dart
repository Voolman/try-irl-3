class ModelPlatform {
  final String id;
  final String icon;
  final String title;
  final String channels;
  final String availableChannels;

  ModelPlatform(
      {
        required this.id,
        required this.icon,
        required this.title,
        required this.channels,
        required this.availableChannels
      }
      );

  bool isValid(){
    var channelsUser = channels.split(", ");
    return availableChannels
        .split(", ")
        .toSet()
        .containsAll(channelsUser);
  }

  String getFullIconUrl(){
    return icon;
  }

  static ModelPlatform fromJson(Map<String, dynamic> json){
    return ModelPlatform(
        id: json["id"],
        icon: json["icon"],
        title: json["title"],
        channels: json["default_channels"],
        availableChannels: json["allow_channels"]
    );
  }

  Map<String, String> toJson(){
    return {
      "id": id,
      "icon": icon,
      "title": title,
      "default_channels": channels,
      "allow_channels": availableChannels
    };
  }
}
