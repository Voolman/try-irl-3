import 'package:intl/intl.dart';
import 'PlatformModels.dart';

class ModelNews {
  final String id;
  final String title;
  final String text;
  final String? media;
  final String type;
  final String channel;
  final String idPlatform;
  final DateTime publishedAt;
  final double? geoLat;
  final double? geoLong;

  ModelNews(
      { required this.id,
        required this.title,
        required this.text,
        this.media,
        required this.type,
        required this.channel,
        required this.idPlatform,
        required this.publishedAt,
        this.geoLat,
        this.geoLong});

  static ModelNews fromJson(Map<String, dynamic> json) {
    return ModelNews(
      id: json['id'],
      title: json["title"],
      text: json["text"],
      media: json["media"],
      type: json["type_media"],
      channel: json["channel"],
      idPlatform: json["id_platform"],
      publishedAt:
      DateFormat("yyyy-MM-ddTHH:mm:ss").parse(json["published_at"]),
      geoLat: json["geo_lat"],
      geoLong: json["geo_long"],
    );
  }

  Map<String, dynamic> toJson(){
    return {
      'id': id,
      'title': title,
      'text': text,
      'media': media,
      'type_media': type,
      'channel': channel,
      'id_platform': idPlatform,
      "published_at": DateFormat('yyyy-MM-ddTHH:mm:ss').format(publishedAt),
      'geo_lat': geoLat,
      'geo_long': geoLong
    };
  }

  ModelPlatform getModelPlatform(List<ModelPlatform> platforms) {
    return platforms.where((element) => element.id == idPlatform).single;
  }

  String formatDate() {
    return DateFormat("dd MMM, HH:mm", "ru")
        .format(publishedAt)
        .replaceAll(".", "");
  }

  String getTitle(int mode) {
    if (mode == 0) {
      return (title.length > 250) ? "${title.substring(0, 250)}..." : title;
    }
    return (title.length > 500) ? "${title.substring(0, 500)}..." : title;
  }

  String getText(int mode) {
    if (mode == 0) {
      return (text.length > 500) ? "${text.substring(0, 500)}..." : text;
    }
    return (text.length > 1500) ? "${text.substring(0, 1500)}..." : text;
  }
}
