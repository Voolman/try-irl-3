import '../models/FavoriteModelNews.dart';
import '../models/ModelNews.dart';
import '../models/PlatformModels.dart';

var testNews = [
  ModelNews(
      id: "id_news_1",
      title: "Title",
      text: "text",
      type: "none",
      channel: "test",
      idPlatform: "id_platform",
      publishedAt: DateTime.now()
  ),
  ModelNews(
      id: "id_news_2",
      title: "Title",
      text: "text",
      type: "none",
      channel: "test",
      idPlatform: "id_platform",
      publishedAt: DateTime.now()
  ),
  ModelNews(
      id: "id_news_3",
      title: "Title",
      text: "text",
      type: "none",
      channel: "test",
      idPlatform: "id_platform",
      publishedAt: DateTime.now()
  ),
  ModelNews(
      id: "id_news_4",
      title: "Title",
      text: "text",
      type: "none",
      channel: "test",
      idPlatform: "id_platform",
      publishedAt: DateTime.now()
  ),
];

var testPlatforms = [
  ModelPlatform(
      id: "id_platform_1",
      icon: "icon",
      title: "Title",
      channels: "test",
      availableChannels: "test"
  ),
  ModelPlatform(
      id: "id_platform_2",
      icon: "icon",
      title: "Title",
      channels: "test",
      availableChannels: "test"
  ),
  ModelPlatform(
      id: "id_platform_3",
      icon: "icon",
      title: "Title",
      channels: "test",
      availableChannels: "test"
  ),
  ModelPlatform(
      id: "id_platform_4",
      icon: "icon",
      title: "Title",
      channels: "test",
      availableChannels: "test"
  ),
];

var testFavoriteNews = [
  FavoriteModelNews(
    dateTimeAdded: DateTime(2003),
    news: ModelNews(
        id: "id_news_1",
        title: "Title",
        text: "text",
        type: "none",
        channel: "test",
        idPlatform: "id_platform",
        publishedAt: DateTime.now()
    ),
    platform: ModelPlatform(
        id: "id_platform_1",
        icon: "icon",
        title: "Title",
        channels: "test",
        availableChannels: "test"
    ),
  ),
  FavoriteModelNews(
    dateTimeAdded: DateTime(2002),
    news: ModelNews(
        id: "id_news_2",
        title: "Title",
        text: "text",
        type: "none",
        channel: "test",
        idPlatform: "id_platform",
        publishedAt: DateTime.now()
    ),
    platform: ModelPlatform(
        id: "id_platform_2",
        icon: "icon",
        title: "Title",
        channels: "test",
        availableChannels: "test"
    ),
  ),
  FavoriteModelNews(
    dateTimeAdded: DateTime(2004),
    news: ModelNews(
        id: "id_news_3",
        title: "Title",
        text: "text",
        type: "none",
        channel: "test",
        idPlatform: "id_platform",
        publishedAt: DateTime.now()
    ),
    platform: ModelPlatform(
        id: "id_platform_3",
        icon: "icon",
        title: "Title",
        channels: "test",
        availableChannels: "test"
    ),
  ),
  FavoriteModelNews(
    dateTimeAdded: DateTime(2005),
    news: ModelNews(
        id: "id_news_4",
        title: "Title",
        text: "text",
        type: "none",
        channel: "test",
        idPlatform: "id_platform",
        publishedAt: DateTime.now()
    ),
    platform: ModelPlatform(
        id: "id_platform_4",
        icon: "icon",
        title: "Title",
        channels: "test",
        availableChannels: "test"
    ),
  ),
];
