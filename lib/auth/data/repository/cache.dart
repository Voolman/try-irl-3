import 'package:training6/auth/data/models/ModelNews.dart';
import 'package:training6/auth/data/models/PlatformModels.dart';
import 'package:training6/auth/data/storage/favorite_news.dart';

import '../models/FavoriteModelNews.dart';




Future<void> removeFavoriteNews(ModelNews news) async {
  favoriteNews = favoriteNews.where(
          (element) => element.news.id != news.id
  ).toList();
}

void addFavoriteNews(ModelNews news, ModelPlatform platform){
  favoriteNews.add(FavoriteModelNews(news: news, platform: platform, dateTimeAdded: DateTime.now()));
}