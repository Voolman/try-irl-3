import 'package:flutter/material.dart';

import '../../../common/app.dart';
import '../../data/models/ModelNews.dart';
import '../../data/models/PlatformModels.dart';

class MediaPage extends StatefulWidget {

  final ModelNews news;
  final ModelPlatform platform;

  const MediaPage({super.key, required this.news, required this.platform});

  @override
  State<MediaPage> createState() => _MediaPageState();
}

class _MediaPageState extends State<MediaPage> {

  Widget getBodyPortrait(){
    var colors = MyApp.of(context).getColorsApp(context);
    return Column(
      children: [
        const SizedBox(height: 57),
        Row(
          children: [
            const SizedBox(width: 22),
            GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: const Icon(Icons.close)
            ),
            const SizedBox(width: 14),
            Image.network(widget.platform.getFullIconUrl(), height: 30, width: 30),
            const SizedBox(width: 12),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                      text: TextSpan(
                          style: TextStyle(color: colors.text),
                          children: [
                            TextSpan(
                                text: "${widget.news.channel} • ",
                                style: const TextStyle(
                                  fontSize: 16,
                                )
                            ),
                            TextSpan(
                                text: widget.platform.title,
                                style: const TextStyle(fontSize: 12)),
                          ])),
                  Text(widget.news.formatDate(),
                      style: TextStyle(color: colors.subtext, fontSize: 10)),
                ],
              ),
            ),
            const Icon(Icons.share_outlined),
            const SizedBox(width: 14),
            const Icon(Icons.bookmark_border_outlined),
            const SizedBox(width: 22),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 22, left: 22, right: 22),
          child: Text(widget.news.title, maxLines: 1, style: TextStyle(
            fontSize:  16,
            overflow: TextOverflow.ellipsis,
            color: colors.text,
          )),
        ),
        Expanded(child: Image.network(widget.news.media!)),
        Padding(
          padding: const EdgeInsets.all(22),
          child: Text(
              widget.news.text,
              maxLines: 5,
              style: TextStyle(fontSize: 12, color: colors.subtext, overflow: TextOverflow.ellipsis)
          ),
        )
      ],
    );
  }

  Widget getBodyLandscape(){
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(width: 22),
        Padding(
          padding: const EdgeInsets.only(top: 22),
          child: GestureDetector(
              onTap: (){
                Navigator.of(context).pop();
              },
              child: const Icon(Icons.close)
          ),
        ),
        Expanded(child: Image.network(widget.news.media!)),
        const Padding(
          padding: EdgeInsets.only(top: 22),
          child: Row(
            children: [
              Icon(Icons.share_outlined),
              SizedBox(width: 14),
              Icon(Icons.bookmark_border_outlined),
              SizedBox(width: 22),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    var isLandscape = MediaQuery.of(context).orientation == Orientation.landscape;
    return Scaffold(
        backgroundColor: colors.background,
        body: (isLandscape) ? getBodyLandscape() : getBodyPortrait()
    );
  }
}
