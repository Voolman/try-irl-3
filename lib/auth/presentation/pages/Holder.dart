import 'package:flutter/material.dart';
import '../../../common/colors.dart';

class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _HolderState();
}
var colors = LightColors();
class _HolderState extends State<Holder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 399, horizontal: 24),
        child: SizedBox(
          height: 46,
          width: double.infinity,
          child: FilledButton(
              onPressed: (){},
              style: Theme.of(context).filledButtonTheme.style,
              child: Text(
                'ВЫХОД',
                style: Theme.of(context).textTheme.labelMedium,
              )
          ),
        ),
      ),
    );
  }
}