import 'dart:io';
import 'package:flutter/material.dart';
import '../../../../common/app.dart';
import '../../../../common/utils.dart';
import '../../../domain/HolderPresenter.dart';
import '../SetupProfile.dart';


class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {


  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.only(top: 73, left: 24, right: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(32),
                  child: Center(child: Image.asset("assets/Vector.png", fit: BoxFit.none, height: 121, width: 121,))


                ),
                const SizedBox(width: 24,),
                Flexible(
                  child: Text(
                    "Смирнов Александр Максимович",
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 18),
                  ),
                )
              ],
            ),
            const SizedBox(height: 28),
            Container(
              color: colors.subtext,
              height: 1,
              width: double.infinity,
            ),
            const SizedBox(height: 25),
            Text(
              'История',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w600,fontSize: 18),
            ),
            const SizedBox(height: 19,),
            Text(
              'Прочитанные статьи',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,fontSize: 14),
            ),
            const SizedBox(height: 26,),
            Text(
              'Чёрный список',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,fontSize: 14),
            ),
            const SizedBox(height: 35,),
            Text(
              'Настройки',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w600,fontSize: 18),
            ),
            const SizedBox(height: 19,),
            GestureDetector(
              onTap: (){
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const SetupProfilePage()), (route) => false
                );

              },
              child: Text(
                'Редактирование профиля',
                style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,fontSize: 14),
              ),
            ),
            const SizedBox(height: 26,),
            Text(
              'Политика конфиденциальности',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,fontSize: 14),
            ),
            const SizedBox(height: 26,),
            Text(
              'Оффлайн чтение',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,fontSize: 14),
            ),
            const SizedBox(height: 26,),
            Text(
              'О нас',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,fontSize: 14),
            ),
            const SizedBox(height: 35,),
            SizedBox(
              width: double.infinity,
              height: 46,
              child: OutlinedButton(
                  style: Theme.of(context).outlinedButtonTheme.style,
                  onPressed: (){
                    pressSignOut(
                        (){exit(0);},
                        (String e){showError(context, e);}
                    );
                  },
                  child: Text(
                    'Выход',
                    style: Theme.of(context).textTheme.labelSmall,
                  )
              ),
            )
          ],
        ),
      ),
    );
  }
}