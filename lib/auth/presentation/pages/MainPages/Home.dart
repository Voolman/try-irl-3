import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:training6/auth/data/models/ModelNews.dart';
import 'package:training6/auth/domain/HomePresenter.dart';
import 'package:training6/auth/presentation/pages/Article.dart';
import 'package:training6/common/app.dart';
import 'package:training6/common/utils.dart';

import '../../../data/models/PlatformModels.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  HomePresenter presenter = HomePresenter();
  var modeLayout = 0;
  List<Widget> items = [];

  @override
  void initState(){
    super.initState();
    presenter.fetchData(
            (res, res2){setState(() {
              var news = res;
              var platform = res2;
              getItems(news, platform);
            });},
        (String e){showError(context, e);}
    );
  }

  Widget getItemsNews(ModelNews news, List<ModelPlatform> platform){
    var colors = MyApp.of(context).getColorsApp(context);
    var platforms = news.getModelPlatform(platform);
    return GestureDetector(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => ArticlePage(news: news, platform: platforms)));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 12),
          Row(
            children: [
              Image.network(platforms.getFullIconUrl(), height: 30, width: 30,),
              SizedBox(width: 12),
              Column(
                children: [
                  RichText(text: TextSpan(
                    style: TextStyle(color: colors.text),
                    children: [
                      TextSpan(
                        text: '${news.channel} • ',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400
                        ),
                      ),
                      TextSpan(
                        text: platforms.title,
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w400
                        ),
                      )
                    ]
                  )),
                  Text(
                    news.formatDate(),
                    style: TextStyle(
                      color: colors.text,
                      fontWeight: FontWeight.w400,
                      fontSize: 10
                    )
                  )
                ],
              )
            ],
          ),
          SizedBox(height: 12),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Flexible(
                child: Text(
                  news.getTitle(modeLayout),
                  style: TextStyle(
                    color: colors.text,
                    fontSize: 14,
                    fontWeight: FontWeight.w500
                  ),
                ),
              ),
              SizedBox(width: 8),
              (news.type == 'photo' && modeLayout == 0) ? SizedBox(
                height: 75,
                child: AspectRatio(
                  aspectRatio: 4/3,
                  child: Image.network(news.media!, fit: BoxFit.cover,),
                ),
              ) : (news.type == 'audio' && modeLayout == 0) ? Container(
                height: 75,
                width: 100,
                child: Image.asset("assets/audio.png"),
              ) : (news.type == 'link' && modeLayout == 0) ? Container(
                height: 75,
                width: 100,
                child: Image.asset("assets/link.png"),
              ) : SizedBox()
            ],
          ),
          SizedBox(height: 12),
          Text(
            news.getText(modeLayout),
            style: TextStyle(
              color: colors.text,
              fontWeight: FontWeight.w400,
              fontSize: 12
            ),
          ),
          SizedBox(height: 12),
          Divider(height: 1, color: colors.text)
        ],
      ),
    );
  }


  void getItems(List<ModelNews> news, List<ModelPlatform> platform){
      items = news.map((e) => getItemsNews(e, platform)).toList();
  }

  void changeLayout(int newIndex){
    setState(() {
      modeLayout = newIndex;
    });
    Navigator.of(context).pop();
    showPickerLayoutDialog();
  }

  void showPickerLayoutDialog(){
    var colors = MyApp.of(context).getColorsApp(context);
    showDialog(context: context, builder: (_) => Dialog(
      surfaceTintColor: Colors.transparent,
      insetPadding: EdgeInsets.symmetric(horizontal: 22),
      backgroundColor: colors.background,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(12))
      ),
      child: SizedBox(
        height: 248,
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.only(top: 18, left: 22, bottom: 28, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'СЕТКА ОТОБРАЖЕНИЯ',
                style: TextStyle(
                  color: colors.subtext,
                  fontSize: 16,
                  fontWeight: FontWeight.w500
                ),
              ),
              SizedBox(height: 15),
              Divider(height: 1, color: colors.subtext),
              SizedBox(height: 28),
              GestureDetector(
                onTap: (){changeLayout(0);},
                child: Row(
                  children: [
                    (modeLayout == 0) ? Image.asset("assets/mode_0_check.png") : Image.asset("assets/mode_0.png"),
                    SizedBox(width: 18),
                    Expanded(
                      child: Text(
                          "Card",
                        style: TextStyle(
                          color: colors.text,
                          fontSize: 18,
                          fontWeight: (modeLayout == 0) ? FontWeight.w700 : FontWeight.w400
                        ),
                      ),
                    ),
                    (modeLayout == 0) ? Image.asset("assets/check.png") : SizedBox(),
                    SizedBox(width: 14)
                  ],
                ),
              ),
              SizedBox(height: 28),
              GestureDetector(
                onTap: (){changeLayout(1);},
                child: Row(
                  children: [
                    (modeLayout == 1) ? Image.asset("assets/mode_1_check.png") : Image.asset("assets/mode_1.png"),
                    SizedBox(width: 18),
                    Expanded(
                      child: Text(
                        "Text",
                        style: TextStyle(
                            color: colors.text,
                            fontSize: 18,
                            fontWeight: (modeLayout == 1) ? FontWeight.w700 : FontWeight.w400
                        ),
                      ),
                    ),
                    (modeLayout == 1) ? Image.asset("assets/check.png") : SizedBox(),
                    SizedBox(width: 14)
                  ],
                ),
              ),
              SizedBox(height: 28),
              GestureDetector(
                onTap: (){changeLayout(2);},
                child: Row(
                  children: [
                    (modeLayout == 2) ? Image.asset("assets/mode_2_check.png") : Image.asset("assets/mode_2.png"),
                    SizedBox(width: 18),
                    Expanded(
                      child: Text(
                        "Media",
                        style: TextStyle(
                            color: colors.text,
                            fontSize: 18,
                            fontWeight: (modeLayout == 2) ? FontWeight.w700 : FontWeight.w400
                        ),
                      ),
                    ),
                    (modeLayout == 2) ? Image.asset("assets/check.png") : SizedBox(),
                    SizedBox(width: 14)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: (modeLayout != 2) ? 22 : 4),
          child: Column(
            children: <Widget>[
              SizedBox(height: 63),
              Row(
                children: [
                  SizedBox(width: (modeLayout == 2) ? 18 : 0),
                  Expanded(
                      child: Text(
                        'Лента новостей',
                        style: TextStyle(
                          color: colors.text,
                          fontSize: 24,
                          fontWeight: FontWeight.w700
                        ),
                      )
                  ),
                  GestureDetector(
                    onTap: (){
                      showPickerLayoutDialog();
                    },
                    child: Image.asset("assets/mode_$modeLayout.png"),
                  )
                ],
              ),
              SizedBox(height: 18)
            ] + items
          ),
        ),
      ),
    );
  }

}
