import 'package:flutter/material.dart';
import 'package:training6/auth/domain/SearckPresenter.dart';
import 'package:training6/common/utils.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import '../../../../common/app.dart';
import '../../../data/models/ModelNews.dart';
import '../../../data/models/PlatformModels.dart';
import '../Article.dart';

class Search extends StatefulWidget {
  const Search({super.key});

  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  int modeLayout = 0;
  int currentIndex = 0;
  List<Widget> items = [];
  PageController controller = PageController(
    viewportFraction: 0.92
  );
  List<ModelNews> news = [];
  List<ModelPlatform> platform = [];

  Widget getItemsNews(ModelNews news, List<ModelPlatform> platform){
    var colors = MyApp.of(context).getColorsApp(context);
    var platforms = news.getModelPlatform(platform);
    return GestureDetector(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => ArticlePage(news: news, platform: platforms)));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 12),
          Row(
            children: [
              Image.network(platforms.getFullIconUrl(), height: 30, width: 30,),
              SizedBox(width: 12),
              Column(
                children: [
                  RichText(text: TextSpan(
                      style: TextStyle(color: colors.text),
                      children: [
                        TextSpan(
                          text: '${news.channel} • ',
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w400
                          ),
                        ),
                        TextSpan(
                          text: platforms.title,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400
                          ),
                        )
                      ]
                  )),
                  Text(
                      news.formatDate(),
                      style: TextStyle(
                          color: colors.text,
                          fontWeight: FontWeight.w400,
                          fontSize: 10
                      )
                  )
                ],
              )
            ],
          ),
          SizedBox(height: 12),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Flexible(
                child: Text(
                  news.getTitle(modeLayout),
                  style: TextStyle(
                      color: colors.text,
                      fontSize: 14,
                      fontWeight: FontWeight.w500
                  ),
                ),
              ),
              SizedBox(width: 8),
              (news.type == 'photo' && modeLayout == 0) ? SizedBox(
                height: 75,
                child: AspectRatio(
                  aspectRatio: 4/3,
                  child: Image.network(news.media!, fit: BoxFit.cover,),
                ),
              ) : (news.type == 'audio' && modeLayout == 0) ? Container(
                height: 75,
                width: 100,
                child: Image.asset("assets/audio.png"),
              ) : (news.type == 'link' && modeLayout == 0) ? Container(
                height: 75,
                width: 100,
                child: Image.asset("assets/link.png"),
              ) : SizedBox()
            ],
          ),
          SizedBox(height: 12),
          Text(
            news.getText(modeLayout),
            style: TextStyle(
                color: colors.text,
                fontWeight: FontWeight.w400,
                fontSize: 12
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState(){
    super.initState();
    fetchDataSearch(
      (res, res1){
        news = res;
        setState(() {
          news = res;
          platform = res1;
        });
      },
        (String e){showError(context, e);}
    );
  }

  List<PlacemarkMapObject> getPlacemarkMapObjects(){
    if (news.isEmpty){
      return [];
    }
    var currentModelNews = news[currentIndex];
    return news.map((e) => PlacemarkMapObject(
        mapId: MapObjectId('marker-${e.id}'),
        point: Point(latitude: e.geoLat!, longitude: e.geoLong!),
      opacity: 1,
      icon: PlacemarkIcon.single(
        PlacemarkIconStyle(image: BitmapDescriptor.fromAssetImage(
            (currentModelNews == e) ? 'assets/red.png' :
            (currentModelNews.publishedAt.difference(e.publishedAt).isNegative) ?
            'assets/green.png' : 'assets/blue.png'
        ),
        scale: 3),
      )
    )
    ).toList();
  }


  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Column(
        children: [
          SizedBox(height: 77,),
          Expanded(
              child: YandexMap(
                mapObjects: getPlacemarkMapObjects(),
              )
          ),
          SizedBox(height: 16,),
          SizedBox(
            height: 255,
            child: PageView.builder(
              padEnds: false,
              controller: controller,
              itemCount: news.length,
              onPageChanged: (newIndex){
                setState(() {
                  currentIndex = newIndex;
                });
              },
              itemBuilder: (_, index){
                return ListView(
                  children: [
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 22),
                      child: getItemsNews(news[currentIndex], platform),
                    ),
                  ],
                );
              },
            )
          )
        ],
      ),
    );
  }
}