import 'package:flutter/material.dart';
import 'package:training6/auth/domain/ArticlePresenter.dart';
import 'package:training6/auth/presentation/pages/Main.dart';

import '../../../common/app.dart';
import '../../data/models/ModelNews.dart';
import '../../data/models/PlatformModels.dart';


class ArticlePage extends StatefulWidget {

  final ModelNews news;
  final ModelPlatform platform;
  BaseArticlePresenter presenter;
  final initialIndex;

  ArticlePage({
    super.key,
    required this.news,
    required this.platform,
    BaseArticlePresenter? presenterParam,
    this.initialIndex = 0
  }): presenter = (presenterParam != null) ? presenterParam : ArticlePresenter();

  @override
  State<ArticlePage> createState() => _ArticlePageState();
}
class _ArticlePageState extends State<ArticlePage> {

  Duration currentDuration = Duration.zero;
  Duration? maxDuration;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    bool isFavorite = widget.presenter.checkIsFavorite(widget.news.id);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.only(top: 63, left: 22, right: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => Main(initialIndex: widget.initialIndex)), (route) => false);
                    },
                    child: const Icon(Icons.arrow_back)
                  ),
                  const SizedBox(width: 14,),
                  Image.network(widget.platform.getFullIconUrl(), height: 30, width: 30,),
                  const SizedBox(width: 12,),
                  Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(text: TextSpan(
                            style: TextStyle(
                              color: colors.text
                            ),
                            children: [
                              TextSpan(
                                text: '${widget.news.channel} • ',
                                style: const TextStyle(
                                  fontSize: 16
                                )
                              ),
                              TextSpan(
                                text: widget.platform.title,
                                style: const TextStyle(
                                  fontSize: 12
                                )
                              )
                            ]
                          )
                          ),
                          Text(
                            widget.news.formatDate(),
                            style: TextStyle(
                              color: colors.subtext,
                              fontSize: 10
                            ),
                          )
                        ],
                      )
                  ),
                  const Icon(Icons.share_outlined),
                  const SizedBox(width: 14,),
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        widget.presenter.pressFavorite(
                            widget.news,
                            widget.platform,
                        ).then((value) => setState(() {
                          isFavorite = value;
                        }));
                      });
                    },
                      child:  Icon((isFavorite) ? Icons.bookmark : Icons.bookmark_border_outlined)
                  )
                ],
              ),
              const SizedBox(height: 18,),
              Text(
                widget.news.title,
                style: TextStyle(
                  color: colors.text,
                  fontSize: 18,
                  fontWeight: FontWeight.w700
                ),
              ),
              const SizedBox(height: 18,),
              Text(
                  widget.news.text,
                style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontWeight: FontWeight.w400
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
