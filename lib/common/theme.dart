import 'package:flutter/material.dart';
import 'colors.dart';

var lightColors = LightColors();
var theme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 24,
      color: lightColors.text
    ),
    titleMedium: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 14,
        color: lightColors.subtext
    ),
    labelMedium: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 16,
        color: lightColors.disableTextAccent
    ),
    labelSmall: TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 16,
      color: lightColors.error
    )
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: lightColors.accent,
      disabledBackgroundColor: lightColors.disableAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      )
    )
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(4),
      borderSide: BorderSide(color: lightColors.subtext, width: 1),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(4),
      borderSide: BorderSide(color: lightColors.subtext, width: 1),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(4),
      borderSide: BorderSide(color: lightColors.error, width: 1),
    ),
  ),
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    selectedLabelStyle: TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 12,
      color: lightColors.accent
    ),
    unselectedLabelStyle: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 12,
        color: lightColors.subtext
    )
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      ),
      side: BorderSide(color: lightColors.error, width: 1)
    )
  )
);

var darkColors = DarkColors();
var darkTheme = ThemeData(
    textTheme: TextTheme(
        titleLarge: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 24,
            color: darkColors.text
        ),
        titleMedium: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 14,
            color: darkColors.subtext
        ),
        labelMedium: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 16,
            color: darkColors.disableTextAccent
        ),
        labelSmall: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 16,
            color: darkColors.error
        )
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            backgroundColor: darkColors.accent,
            disabledBackgroundColor: darkColors.disableAccent,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
            )
        )
    ),
    inputDecorationTheme: InputDecorationTheme(
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(4),
        borderSide: BorderSide(color: darkColors.subtext, width: 1),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(4),
        borderSide: BorderSide(color: darkColors.subtext, width: 1),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(4),
        borderSide: BorderSide(color: darkColors.error, width: 1),
      ),
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
        selectedLabelStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 12,
            color: darkColors.accent
        ),
        unselectedLabelStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 12,
            color: darkColors.subtext
        )
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
            ),
            side: BorderSide(color: darkColors.error, width: 1)
        )
    )
);