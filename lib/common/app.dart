import 'package:flutter/material.dart';
import 'package:training6/auth/presentation/pages/LogIn.dart';
import '../auth/presentation/pages/SignUp.dart';
import 'colors.dart';
import 'theme.dart';

class MyApp extends StatefulWidget {
  final Widget body;
  MyApp({super.key, Widget? body}) : body  = (body != null) ? body : const LogIn();

  var isLight = true;

  void changeTheme(BuildContext context){
    isLight = !isLight;
    context.findAncestorStateOfType<_MyAppState>()?.onChangedTheme();
  }

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  ColorsApp getColorsApp(BuildContext context){
    return (isLight) ? lightColors : darkColors;
  }

  ThemeData getCurrentTheme(){
    return (isLight) ? theme : darkTheme;
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>{
  void onChangedTheme(){
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: widget.getCurrentTheme(),
      home: const SignUp(),
    );
  }
}