import 'package:flutter/material.dart';

void showError(BuildContext context, String e){
  showDialog(context: context, builder: (_) => AlertDialog(
    title: const Text("Ошибка"),
    content: Text(e),
    actions: [
      TextButton(
          onPressed: (){Navigator.of(context).pop();},
          child: Text("OK")
      )
    ],
  )
  );
}
