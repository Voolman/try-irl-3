import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'common/app.dart';

Future<void> main() async {
  await initializeDateFormatting("ru");
  await Supabase.initialize(
    url: 'https://hawusttfcqcomedevycj.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imhhd3VzdHRmY3Fjb21lZGV2eWNqIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDk3MzQwNDIsImV4cCI6MjAyNTMxMDA0Mn0.FaTVbgSsA1uzIp5miFfUdTH4-G7cnIL9CwGs0rOJ1Hs',
  );

  runApp(MyApp());
}


class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
